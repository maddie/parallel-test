import React, { useState, useEffect } from 'react';

import { useAppSelector, useAppDispatch } from '../store/hooks';
import {
  startRace,
  loadAnts,
  selectAnts,
} from '../store/slices';

export function AntRaceTable() {
  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(loadAnts());
  }, [])
  const ants = useAppSelector(selectAnts);

  return (
    <div className="px-4 sm:px-6 lg:px-8">
      <div className="sm:flex sm:items-center">
        <div className="sm:flex-auto">
          <h1 className="text-xl font-semibold text-gray-900">Ants</h1>
          <p className="mt-2 text-sm text-gray-700">
            Do you want ants? That's how you get ants. (TODO: random quotes about ants)
          </p>
        </div>
      </div>
      <div className="mt-8 flex flex-col">
        <div className="-my-2 -mx-4 overflow-x-auto sm:-mx-6 lg:-mx-8">
          <div className="inline-block min-w-full py-2 align-middle md:px-6 lg:px-8">
            <div className="overflow-hidden shadow ring-1 ring-black ring-opacity-5 md:rounded-lg">
              <table className="min-w-full divide-y divide-gray-300">
                <thead className="bg-gray-50">
                  <tr>
                    <th scope="col" className="py-3.5 pl-4 pr-3 text-left text-sm font-semibold text-gray-900 sm:pl-6">
                      Name
                    </th>
                    <th scope="col" className="px-3 py-3.5 text-sm font-semibold text-gray-900">
                      Length
                    </th>
                    <th scope="col" className="px-3 py-3.5 text-sm font-semibold text-gray-900">
                      Color
                    </th>
                    <th scope="col" className="px-3 py-3.5 text-sm font-semibold text-gray-900">
                      Weight
                    </th>
                    <th scope="col" className="px-3 py-3.5 text-sm font-semibold text-gray-900">
                      Chance
                    </th>
                  </tr>
                </thead>
                <tbody className="divide-y divide-gray-200 bg-white">
                  {ants.map((ant) => (
                    <tr key={ant.name}>
                      <td className="whitespace-nowrap px-3 py-4 text-left text-sm font-medium text-gray-900 sm:pl-6">
                        {ant.name}
                      </td>
                      <td className="whitespace-nowrap px-3 py-4 text-center text-sm text-gray-500">{ant.length}</td>
                      <td className="whitespace-nowrap px-3 py-4 text-sm text-gray-500">{ant.color}</td>
                      <td className="whitespace-nowrap px-3 py-4 text-sm text-gray-500">{ant.weight}</td>
                      <td className="whitespace-nowrap px-3 py-4 text-sm text-gray-500">{(ant.chanceOfWinning || 0) * 100}%</td>
                    </tr>
                  ))}
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}


