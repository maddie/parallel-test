import React, { useState } from 'react';
import antAvatar from '../ant.svg'
import { useAppSelector, useAppDispatch } from '../store/hooks';
import {
  startRace,
  selectAnts,
} from '../store/slices';

export function AntRaceHeader() {
  const dispatch = useAppDispatch();

  const ants = useAppSelector(selectAnts);

  return (
    <div>
      <div className="border-b border-gray-200 pb-5 sm:flex sm:items-center sm:justify-between">
        <h3 className="text-lg font-medium leading-6 text-gray-900">Ant Race Progress</h3>
        <div className="mt-3 sm:mt-0 sm:ml-4">
          <button type="button" className="inline-flex items-center rounded-md border border-transparent bg-indigo-600 px-4 py-2 text-sm font-medium text-white shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2" onClick={() => dispatch(startRace())}>Start Race</button>
        </div>
      </div>
      <div>
        <div className="flex h-96 flex-col w-full">
          {ants.map((ant) => (
            <div key={ant.name} className="relative flex flex-col w-18" style={{left: `${(ant.chanceOfWinning || 0) * 100}%`}}>
              <div className="w-fit z-10">{ant.name} {ant.chanceOfWinning && `(${Math.round(ant.chanceOfWinning * 100)}%)` || ``}</div>
              <img className="h-10 w-10" src={antAvatar} />
            </div>
          ))}
        <div className="h-96 z-0 absolute left-20 border-l-4">
        </div>
        <div className="h-96 z-0 absolute right-20 border-r-4 border-dashed">
        </div>

        </div>

      </div>
    </div>
  );
}

