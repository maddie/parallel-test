import { createAsyncThunk, createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState, AppThunk } from '../';
import { Ant } from '../../types'

export interface AntsState {
  list: Ant[];
  status: 'idle' | 'loading' | 'failed';
}

const initialState: AntsState = {
  list: [],
  status: 'idle',
};

function generateAntWinLikelihoodCalculator() {
  const delay = 7000 + Math.random() * 7000;
  const likelihoodOfAntWinning = Math.random();

  return (callback: (chance: number) => void) => {
    setTimeout(() => {
      callback(likelihoodOfAntWinning);
    }, delay);
  };
}

export const startRace = createAsyncThunk(
  'ants/start-race',
  async (a, { getState, dispatch }) => {
    // @reviewer-ignore (I was running out of time :p)
    // @ts-ignore
    const ants = selectAnts(getState())

    ants.map(async (ant) => {
      const callFn = generateAntWinLikelihoodCalculator()
      callFn((winChance: number) => {
        dispatch(setAntWinChance({ name: ant.name, chance: winChance }))
      })
    })
  }
)

export const loadAnts = createAsyncThunk(
  'ants/load',
  async () => {
    return [
            {
                "name": "Marie ‘Ant’oinette",
                "length": 12,
                "color": "BLACK",
                "weight": 2,
                "chanceOfWinning": 0,
            },
            {
                "name": "Flamin’ Pincers",
                "length": 11,
                "color": "RED",
                "chanceOfWinning": 0,
                "weight": 2
            },
            {
                "name": "AuNT Sarathi",
                "length": 20,
                "color": "BLACK",
                "chanceOfWinning": 0,
                "weight": 5
            },
            {
                "name": "The Unbeareable Lightness of Being",
                "length": 5,
                "color": "SILVER",
                "chanceOfWinning": 0,
                "weight": 1
            },
            {
                "name": "‘The Duke’",
                "length": 17,
                "color": "RED",
                "chanceOfWinning": 0,
                "weight": 3
            }
        ]
  }
);

interface SetAntChance {
  name: string;
  chance: number;
}

export const antsSlice = createSlice({
  name: 'ants',
  initialState,
  reducers: {
    setAntWinChance: (state, action: PayloadAction<SetAntChance>) => {
      const antToUpdate = state.list.find((ant) => ant.name === action.payload.name)

      if (!antToUpdate) return;

      antToUpdate.chanceOfWinning = action.payload.chance
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(loadAnts.pending, (state) => {
        state.status = 'loading';
      })
      .addCase(loadAnts.fulfilled, (state, action) => {
        state.status = 'idle';
        state.list = action.payload
      })
      .addCase(loadAnts.rejected, (state) => {
        state.status = 'failed';
      });
  },
});

export const { setAntWinChance } = antsSlice.actions;

export const selectAnts = (state: RootState) => {
  return state.ants.list
};

export default antsSlice.reducer;

