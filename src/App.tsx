import React from 'react';
import logo from './logo.svg';
import { AntRaceHeader } from './modules/AntRaceHeader';
import { AntRaceTable } from './modules/AntRaceTable';
import './App.css';

function App() {
  return (
    <div className="App">
      <div className="w-full h-25">
        <AntRaceHeader/>
      </div>
      <div className="w-full h-50">
        <AntRaceTable/>
      </div>
    </div>
  );
}

export default App;
