// TODO not exhaustive. there may be ants of other colors. :D
enum ANT_COLOR {
  BLACK,
  RED,
  SILVER,
}

export interface Ant {
  name: string;
  length: number;
  weight: number;
  color: ANT_COLOR|string;
  chanceOfWinning: number;
}

